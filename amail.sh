#!/bin/bash

# >> amail.sh <<
#
# Arkanon <arkanon@lsd.org.br>
# 2024/07/25 (Thu) 01:58:48 -03
# 2023/06/12 (Mon) 16:54:05 -03
# 2023/05/25 (Thu) 17:56:25 -03
# 2022/11/16 (Wed) 04:52:27 -03
# 2022/07/15 (Fri) 12:26:11 -03
# 2021/12/10 (Fri) 15:21:56 -03
# 2021/12/08 (Wed) 12:19:39 -03
# 2021/12/07 (Tue) 12:06:25 -03
# 2021/03/23 (Tue) 04:03:51 -03
# 2021/03/16 (Tue) 22:01:56 -03
# 2020/08/03 (Mon) 13:21:22 -03
# 2020/04/28 (Tue) 18:22:04 -03
# 2019/05/29 (Wed) 16:19:27 -03
# 2018/09/25 (Tue) 15:00:05 -03
# 2018/09/24 (Mon) 08:40:48 -03
# 2018/08/03 (Fri) 03:15:44 -03
# 2018/08/02 (Thu) 20:47:28 -03
# 2017/05/09 (Tue) 08:15:43 BRS
# 2017/04/06 (Thu) 16:48:13 BRS
# 2017/03/26 (Sun) 23:23:04 BRS
# 2017/03/24 (Fri) 16:33:48 BRS
# 2016/10/13 (Thu) 19:56:40 BRD
# 2016/05/16 (Mon) 10:52:55 BRS
# 2016/05/13 (Fri) 12:56:54 BRS
# 2016/05/04 (Wed) 03:34:20 BRS
# 2016/05/03 (Tue) 16:47:28 BRS
# 2016/05/02 (Mon) 03:16:52 BRS
# 2016/05/02 (Mon) 00:18:12 BRS
#
# Envia HTML mail para múltiplos destinatários em cc ou bcc com imagens inline e anexos usando
#   smtp privado ou público (ex: gmail) com endereço do remetente configurável.
#
# >> DEPENDÊNCIAS <<
#    msmtp     pacote msmtp <http://msmtp.sf.net>
#    base64    pacote coreutils
#  # uuencode  pacote sharutils
#  # uuidgen   pacote util-linux-ng
#
# >> TODO <<
#    ok detectar dependências
#    -- usando sendgrid, texto do campo From não dev mais ser codificado em base 64 e encapsulado como utf-8
#    -- parametrizar tipo de mensagem (texto plano/html)
#    -- adicionar dominio default
#    ok utilizar identificador de imagem inline igual ao nome do arquivo
#    ok remover endereços repetidos no mesmo campo
#    ok usar servidor interno sem autenticação
#    ok usar conta do gmail
#    ok forçar nome/endereço do remetente (from)
#    ok aceitar utf-8 nos campos to/cc/bcc/subject
#    ok aceitar utf-8 no corpo do email
#    ok parametrizar em arquivo de configuração
#    ok enviar msg html com imagens inline e de fundo
#    ok enviar msg com anexos

# >> ------------------------------------------------------------- << #



  test_pkg()
  {
    local cmd pkg why pkm m1 m
    cmd=${1##*/}   # comando necessário
    pkg=${2:-$cmd} # pacote contendo o comando. se o valor for -, não há pacote correspondente e o comando deve ser instalado de alguma outra forma
    why=${3:-I}    # mensagem explicando porque instalar ou não o pacote
    pkm=unsupported
    m1="${why}nstale-o da forma mais adequada."
    for i in apt-get apt yum dnf; { which $i &> /dev/null && pkm=$i; }
    if ! which $cmd &> /dev/null
    then
      if [[ $pkm == unsupported ]]
      then
        m="O formato de empacotamento do seu sistema não é deb ou rpm. $m1"
      else
        [[ $pkg == - ]] && m=$m1 || m="${why}nstale o pacote $pkg:\n\n  sudo $pkm -y install $pkg"
      fi
      echo "Comando '${cmd##*/}' não encontrado no PATH. $m\n\n"
    fi
  }

  msg+=$( test_pkg file   )
  msg+=$( test_pkg msmtp  )
  msg+=$( test_pkg base64 )
  [[ $msg ]] && { echo -en "\n$msg"; exit; }

  declare -x TIMEFORMAT=%lR
  declare -A valor



  dconff=$PWD/amail.sh.conf

  [[ $1 == -h || ! $1 && ! -e $dconff ]] && { echo -e "\nUso: ${0##*/} [arquivo de configuração]\n\n  O arquivo de configuração assumido como default é $dconff\n"; exit; }

  [[ $1 ]] && aconff=$(readlink -f $1) || aconff=$dconff

  [[ -e $aconff ]] || echo "Arquivo de configuração [$aconff] não encontrado."

  PREFIX=${aconff%/*}

  . $aconff

   logf=${logf:-$PREFIX/logs/amail.log}
  talkf=${talkf:-$PREFIX/logs/amail-talk.txt}
  bodyf=${bodyf:-$PREFIX/logs/amail-body.html}
   srcf=${srcf:-$PREFIX/logs/amail-src.txt}



  mkdir -p ${logf%/*}

# >> ------------------------------------------------------------- << #

  nohtm="Por favor, habilite a leitura de HTML Mail."

  part_id="aaaaaaaaaaaaaaaaaaaaaaaaaaaa" # $(uuidgen)
  body_id="bbbbbbbbbbbbbbbbbbbbbbbbbbbb" # $(uuidgen)

# >> ------------------------------------------------------------- << #



  _from=$(sed -r 's/[^<]*< *([^<>]+) *>.*/\1/; s/ //g' <<< $from) # filtra apenas o endereço do valor de $from



  if [[ $config_file ]]
  then

      conf=$(<$config_file)
    xparms=(
             --logfile=$logf
             --account=$account
           )

  else

    conf="
           from    $_from
           logfile $logf
           host    $smtp
         "

    if [[ $acct ]]
    then
      auth="auth login"
      (( port == 587 )) && \
      auth="
             auth
             tls
             tls_certcheck off
           "
      conf="
             $conf
             $auth
             port     $port
             user     $acct
             password $pass
           "
    fi

  fi



  attach()
  {
  # parametro disposition
  # global    file part_id
  # retorna   encoding_header encoded_file
    disp=$1
    file=${path##*/}
    file=${file// /+}
    type=$(file -b --mime-type $path)
    echo "--$part_id"
    echo "Content-Type:"              "$type; name=\"$file\""
    echo "Content-Disposition:"       "$disp; filename=\"$file\""
    echo "Content-Transfer-Encoding:" "base64"
    if [[ $disp == inline ]]
    then
      echo "Content-ID:"              "<$file>"
      echo "X-Attachment-Id:"         "$file"
    fi
    echo
    base64 < $path
    echo $file anexado como $disp >&2
  }

  mkaddr()
  {
    sed='
          s/^ +| +$//g # remove espaços no inicio e no fim da string
          s/ *, */,/g  # remove espaços antes e depois de ,
          s/ +/ /g     # substitui espaços em sequencia por apenas um
          :loop ; s/<([^>]*) /<\1/ ; t loop # em loop, remove todas as ocorrências de espaço entre < e >
          s/(^|,)([^,<]+) /\1=?utf-8?B?\$(base64 -w0 <<< "\2")?= /g # deixa a lista de emails na forma [ =?utf-8?B?$(base64 -w0 <<< "nome")?= <endereco@dominio> ]
        '
    v="$1[*]"
    out=$(IFS=,; echo -e "${!v}" | sed -r "$sed")
    eval echo \"$out\"
  }

# >> ------------------------------------------------------------- << #

  {

    echo "From:"                      $(mkaddr from)
  # echo "From:"                      $_from         # sendgrid não aceita mais texto do campo From em base 64 e encapsulamento utf-8
    echo "To:"                        $(mkaddr to  )
    echo "CC:"                        $(mkaddr cc  )
    echo "BCC:"                       $(mkaddr bcc )

    echo "Subject:"                   "=?utf-8?B?$(echo -e "$assunto" | base64 -w0)?="
    echo "MIME-Version:"              "1.0"
    echo "Content-Type:"              "multipart/related; boundary=$part_id"
    echo

    echo "--$part_id"
    echo "Content-Type:"              "multipart/alternative; boundary=$body_id"
    echo

    echo "--$body_id"
    echo "Content-Type:"              "text/plain; charset=UTF-8"
    echo
    echo "$nohtm"

    echo "--$body_id"
    echo "Content-Type:"              "text/html; charset=UTF-8"
    echo "Content-Disposition:"       "inline"
    echo "Content-Transfer-Encoding:" "base64"
    echo

    IFo=$IFS
    IFS=$'\n'
    [[ -f $corpo ]] && corpo=$(<"$corpo")
    for path  in ${inline[*]} ; { file=${path##*/}; corpo="$(sed -r "s/$file/cid:${file// /+}/g"                    <<< "$corpo" )" ; }
    IFS=$IFo
    for campo in ${!valor[*]} ; {                   corpo="$(sed -r "s%$campo%${valor[$campo]//$'\n'/§}%g;s/§/\n/g" <<< "$corpo" )" ; }

    echo "$corpo" >| $bodyf
    base64 <<< "$corpo"
    echo "--$body_id--"

    IFS=$'\n'
    for path in "${inline[@]}" ; { attach inline     ; }
    for path in "${anexo[@]}"  ; { attach attachment ; }
    IFS=$IFo

    echo "--$part_id--"

  } | tee $srcf | { msmtp -d -t -C <(echo "$conf") "${xparms[@]}" >| $talkf 2>&1 ; } || { echo -e "\nErro. Inspecione os arquivos\n\n$talkf\n$logf\n"; tail -n1 $logf; echo; }

# >> ------------------------------------------------------------- << #



# EOF
