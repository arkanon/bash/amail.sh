#!/bin/bash

# Arkanon <arkanon@lsd.org.br>
# 2022/07/19 (Tue) 04:53:04 -03

  . $ADM_DIR/.s-m

        to=arkanon@lsd.org.br
      smtp=smtps://smtp.gmail.com:465
      file=logo.png
  mimetype=$(file --mime-type "$file" | sed 's/.*: //')
   subject=Teste
      body="Isso é um teste."

  curl        \
  -s          \
  --url       "$smtp" \
  --ssl-reqd  \
  --mail-from $user \
  --mail-rcpt $to\
  -u          $user:$pass \
  -H          "Subject: $subject" \
  -H          "From: $user" \
  -H          "To: $to" \
  -F          "=(;type=multipart/mixed" \
  -F          "=$body;type=text/plain" \
  -F          "file=@$file;type=$mimetype;encoder=base64" \
  -F          "=)"

# EOF
