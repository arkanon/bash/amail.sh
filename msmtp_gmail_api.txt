# vim: ft=sh

# Arkanon <arkanon@lsd.org.br>
# 2022/11/28 (Mon) 04:31:20 -03
# 2022/11/16 (Wed) 05:00:53 -03
# 2022/08/17 (Wed) 05:23:58 -03
# 2022/08/08 (Mon) 13:26:38 -03
# 2022/08/01 (Mon) 13:24:12 -03

# Conta no GMail
#
# http://myaccount.google.com/lesssecureapps
# http://accounts.google.com/b/0/DisplayUnlockCaptcha



# [ATENÇÃO]
# -- as variáveis gmail_*, a chave privada, o certificado de revogação e o token de acesso ficam armazenados no gitlab como variáveis de CI/CD,
#    exceto a variável msmtp_gmail_user, cujo valor é obtido junto com o fingeprint a partir chave privada
#
# -- em um novo conjunto de arquivos, armazenar no gitlab o token de acesso em base 64 (*.auth.b64) como variável
#    e a chave privada e o certificado de revogação como arquivo



  sudo apt install -y jq gnupg curl uuid



  msmtp_dir=$HOME/tmp/msmtp

  msmtp_gmail_name="pdi-ce"
  msmtp_gmail_comm="Personal Key"
  msmtp_gmail_klen=4096



  [[ $BASH_VERSION =~ ^(5\.(1[0-9]|[1-9])|1[0-9]|[6-9]) ]]; D=$? # BASH_VERSION >= 5.1

### [1. criação da conta] <http://accounts.google.com/signup>
# msmtp_gmail_user=$(uuid|tr -d -|cut -c-30)@gmail.com; LC_ALL=C printf "msmtp_gmail_user=$msmtp_gmail_user; history -d \$((HISTCMD-D)) # %(%Y-%m-%d %a %H:%M:%S %Z)T\n"
  msmtp_gmail_user=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx@gmail.com; history -d $((HISTCMD-D)) # 2022-08-01 Mon 13:24:12 -03

# msmtp_gmail_pasw=$(uuid); LC_ALL=C printf "msmtp_gmail_pasw=$msmtp_gmail_pasw; history -d \$((HISTCMD-D)) # %(%Y-%m-%d %a %H:%M:%S %Z)T\n"
  msmtp_gmail_pasw=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx; history -d $((HISTCMD-D)) # 2022-11-16 Wed 00:33:19 GMT

# msmtp_gmail_pasp=$(uuid); LC_ALL=C printf "msmtp_gmail_pasp=$msmtp_gmail_pasp; history -d \$((HISTCMD-D)) # %(%Y-%m-%d %a %H:%M:%S %Z)T\n"
  msmtp_gmail_pasp=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx; history -d $((HISTCMD-D)) # 2022-08-01 Mon 13:24:12 -03



### [2. cadastro do app/cliente oauth] <http://console.cloud.google.com/projectselector2/apis/dashboard>
      msmtp_gmail_client_id=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.apps.googleusercontent.com; history -d $((HISTCMD-D))
  msmtp_gmail_client_secret=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx; history -d $((HISTCMD-D))



### [ATENÇÃO] o aplicativo oauth deve ser colocado em produção, caso contrário o toke nde acesso expirará em 2 semanas
#             <http://stackoverflow.com/questions/67452796/google-api-oauth2-refresh-tokens-abruptly-revoked>



### [3. download do o mailctl] <http://github.com/pdobsan/mailctl>
    ns=pdobsan/mailctl
   url=https://api.github.com/repos/$ns/releases/latest
  pref=https://github.com/$ns/raw/main/configs
   url=$(curl -Ls $url | jq -r '.assets[].browser_download_url' | grep x86_64.tgz)
  curl -Ls $url | tar zxvf - --strip-components 1 --wildcards */mailctl
  sudo mv mailctl /usr/local/bin

# [[ -e msmtp-gmail   ]] || curl -sL $pref/msmtp-config-template  -o msmtp-gmail
# [[ -e gmail.yaml    ]] || curl -sL $pref/config-template.yaml   -o gmail.yaml
# [[ -e services.yaml ]] || curl -sL $pref/services-template.yaml -o services.yaml



  # <http://gnupg.org/documentation/manuals/gnupg/Unattended-GPG-key-generation.html>
# curl -s https://gnupg.org/signature_key.asc | gpg --import -
  ps -ef | grep [g]pg && gpgconf --kill gpg-agent



### [4. geração da chave privada]
  # <http://github.com/pdobsan/mailctl#configuration>
  # <http://wiki.archlinux.org/title/msmtp#OAuth2_Setup>

  export GNUPGHOME=$msmtp_dir/gnupg
  rm    -rf    ${GNUPGHOME?}
  mkdir -pm 700 $GNUPGHOME

  gpg --batch --gen-key <<< "
    Key-Type:      1
    Subkey-Type:   1
    Key-Length:    $msmtp_gmail_klen
    Subkey-Length: $msmtp_gmail_klen
    Name-Real:     $msmtp_gmail_name
    Name-Comment:  $msmtp_gmail_comm
    Name-Email:    $msmtp_gmail_user
    Passphrase:    $msmtp_gmail_pasp
    Expire-Date:   0
  "

  # gpg: keybox '$GNUPGHOME/pubring.kbx' created
  # gpg: $GNUPGHOME/trustdb.gpg: banco de dados de confiabilidade criado

  # gpg: chave $fingerprint marcada como plenamente confiável
  # gpg: directory '$GNUPGHOME/openpgp-revocs.d' created
  # gpg: revocation certificate stored as '$GNUPGHOME/openpgp-revocs.d/8EB5FEAEFC126304E5D896B857E43B0CCC217EB7.rev'

  gpg --list-signatures | grep sig | grep -oE '[[:xdigit:]]{16} [0-9]{4}(-[0-9]{2}){2} .*' | sort -u
  # $fingerprint 2022-11-15  $msmtp_gmail_name ($msmtp_gmail_comm) <$msmtp_gmail_user>



### [5. exportação da chave na base do gnupg para arquivos separados: chave privada, pública e certificado de revogação]
  read  fingerprint x < <(gpg --list-signatures | grep sig | grep -oE "[[:xdigit:]]{16} [0-9]{4}(-[0-9]{2}){2}  $msmtp_gmail_name" | sort -u)
  echo $fingerprint
# fingerprint=xxxxxxxxxxxxxxxx; history -d $((HISTCMD-D))

  cp -a $GNUPGHOME/openpgp-revocs.d/*$fingerprint.rev                                               $fingerprint.rev
  gpg                                                         --export            -a $fingerprint > $fingerprint.pub
  gpg --pinentry-mode loopback --passphrase $msmtp_gmail_pasp --export-secret-key -a $fingerprint > $fingerprint.key
  chmod 600 $fingerprint.*



### [6. importação da chave privada, pública e certificado de revogação para a base do gnupg]
  rm    -rf    ${GNUPGHOME?}
  mkdir -pm 700 $GNUPGHOME

  # [apenas a chave pública]
# gpg --import $fingerprint.pub
  # gpg: chave $fingerprint: chave pública "$msmtp_gmail_name ($msmtp_gmail_comm) <$msmtp_gmail_user>" importada
  # gpg: Número total processado: 1
  # gpg:               importados: 1

  # [chaves pública e privada]
  gpg --pinentry-mode loopback --passphrase $msmtp_gmail_pasp --import $fingerprint.key
  # gpg: chave $fingerprint: chave pública "$msmtp_gmail_name ($msmtp_gmail_comm) <$msmtp_gmail_user>" importada
  # [ou, se a chave pública já tiver sido importada]
  # gpg: chave $fingerprint: "$msmtp_gmail_name ($msmtp_gmail_comm) <$msmtp_gmail_user>" não mudada
  #
  # gpg: chave $fingerprint: chave secreta importada
  # gpg: Número total processado: 1
  # gpg:               importados: 1
  # gpg:       chaves secretas lidas: 1
  # gpg:   chaves secretas importadas: 1



### [ATENÇÃO] se a chave não for corretamente importada devido à passphrase errada/vazia ou outro erro qualquer,
###           nos comandos abaixo, mesmo com a passphrase correta, será apresentado o erro
###           [gpg: descriptografia falhou: Nenhuma chave secreta]



  # [chaves existentes na base gnupg]
  gpg --list-keys | grep --color -E '|[[:xdigit:]]{16}$'
  # $GNUPGHOME/pubring.kbx
  # -----------------------------------------
  # pub   rsa2048 2022-11-15 [SCEA]
  #       8EB5FEAEFC126304E5D896B8[$fingerprint]
  # uid           [ desconhecida] $msmtp_gmail_name ($msmtp_gmail_comm) <$msmtp_gmail_user>
  # sub   rsa2048 2022-11-15 [SEA]

       fingerprint=$(gpg --list-keys | grep --color -oE '[[:xdigit:]]{16}$')
  msmtp_gmail_user=$(gpg --list-keys | grep --color -oE '[^<]+@[^>]+')
  echo $fingerprint $msmtp_gmail_user

  # [data de criação do certificado armazenado em um arquivo]
  ts=$(gpg --list-packet $fingerprint.key | grep -oP 'created \K[0-9]{5,}' | sort -u)
  LC_ALL=C printf '%(%Y/%m/%d %a %H:%M:%S %z)T\n' $ts
  # 2022/11/15 Tue 18:19:08 -0300



### [7. configuração do msmtp para autenticação por oauth2 via mailctl]

  cat << EOT > services.yaml

google:
  auth_endpoint: https://accounts.google.com/o/oauth2/auth
  auth_http_method: POST
  auth_params_mode: query-string
  token_endpoint: https://accounts.google.com/o/oauth2/token
  token_http_method: POST
  token_params_mode: both
  redirect_uri: http://localhost:8080
  auth_scope: https://mail.google.com/
  client_id: $msmtp_gmail_client_id
  client_secret: $msmtp_gmail_client_secret

EOT



  cat << EOT > gmail.yaml

# The primary purpose of mailctl is providing IMAP/SMTP clients with the
# capabilities of renewal and authorization of OAuth2 credentials.
# Accordingly only the OAuth2 related config entries are mandatory.

# These four entries should be use for OAuth2 access method
services_file: $msmtp_dir/services.yaml
oauth2_dir: $msmtp_dir

# This encryption method using gnupg.
encrypt_cmd:
  exec: gpg
  args:
    - --homedir
    - $GNUPGHOME
    - --encrypt
    - --recipient
    - $fingerprint
    - --yes
    - --output

decrypt_cmd:
  exec: gpg
  args:
    - --homedir
    - $GNUPGHOME
    - --pinentry-mode
    - loopback
    - --passphrase
    - $msmtp_gmail_pasp
    - --decrypt

EOT



  cat << EOT > msmtp.conf

  defaults
  logfile        $msmtp_dir/log
  auto_from      off
  from           no-reply@lsd.org.br
  tls            on
  tls_trust_file /etc/ssl/certs/ca-certificates.crt
  port           587

  account        gmail
  auth           oauthbearer
  host           smtp.gmail.com
  user           $msmtp_gmail_user
  passwordeval   mailctl -c $msmtp_dir/gmail.yaml access $msmtp_gmail_user

  account        default: gmail

EOT



  chmod 600 services.yaml gmail.yaml msmtp.conf



### [8. optenção do token de acesso para autenticação por oauth2]

  grep -q $msmtp_dir <<< $PATH || PATH=$PATH:$msmtp_dir

  mailctl -c gmail.yaml authorize google $msmtp_gmail_user
  # [no terminal]
  #
  # To grant OAuth2 access to $msmtp_gmail_user visit the local URL below with your browser.
  # http://localhost:8080/start
  # Authorization started ...
  # Hit <Enter> when it has been completed -->
  # gpg: [$fingerprint]: Não há nenhuma garantia que esta chave pertence ao usuário nomeado
  #
  # sub  rsa2048/$fingerprint 2022-11-15 $msmtp_gmail_name ($msmtp_gmail_comm) <$msmtp_gmail_user>
  #  Impressão digital da chave primária: 8EB5 FEAE FC12 6304 E5D8  96B8  57E4 3B0C CC21 7EB7
  #       Impressão digital da sub-chave: 25EB 7801 4D39 39A0 BBCB  15F7 [$fin ger_ prin t___]
  #
  # Não se tem certeza de que esta chave pertence a pessoa listada
  # no identificador de usuário. Se você *realmente* sabe o que está
  # fazendo, pode responder sim à próxima pergunta
  #
  # Usar esta chave de qualquer maneira? (y/N) y
  #
  # Now try to fetch some email!

  # [no browser]
  #
  # Received new refresh and access tokens for $msmtp_gmail_user
  # They have been saved encrypted in $msmtp_dir/$msmtp_gmail_user.auth
  # You may now quit the waiting mailctl program.

  # [resumo]
  #
  # acessar                                         http://localhost:8080/start
  # login com                                       $msmtp_gmail_user
  # autenticação com senha                          $msmtp_gmail_pasw
  # 'O Google não verificou este app'               Continuar
  # 'O app pdi-ce quer acessar sua Conta do Google' Continuar
  #
  # Received new refresh and access tokens for      $msmtp_gmail_user
  # They have been saved encrypted in               $msmtp_dir/$msmtp_gmail_user.auth
  # You may now quit the waiting mailctl program.   Enter

  gpg --list-packet $msmtp_gmail_user.auth
  # gpg: criptografado com 2048-bit RSA chave, ID $fingerprint, criado 2022-11-15
  #       "$msmtp_gmail_name ($msmtp_gmail_comm) <$msmtp_gmail_user>"
  # # off=0 ctb=85 tag=1 hlen=3 plen=268
  # :pubkey enc packet: version 3, algo 1, keyid $fingerprint
  #         data: [2047 bits]
  # # off=271 ctb=d2 tag=18 hlen=3 plen=508 new-ctb
  # :encrypted data packet:
  #         length: 508
  #         mdc_method: 2
  # # off=293 ctb=a3 tag=8 hlen=1 plen=0 indeterminate
  # :compressed packet: algo=2
  # # off=295 ctb=cb tag=11 hlen=2 plen=0 partial new-ctb
  # :literal data packet:
  #         mode b (62), created 1669617834, name="",
  #         raw data: unknown length

  { cat $msmtp_dir/$msmtp_gmail_user.auth | base64 -w0; echo; } > $msmtp_gmail_user.auth.b64



### [9. como a chave gpg privada é auto assinada], o mailctl alerta interativamente sobre a validade dela, então precisa ser especificado que ela é confiável
  access_token_fingerprint=$(gpg --pinentry-mode loopback --passphrase $msmtp_gmail_pasp --list-packet $msmtp_dir/$msmtp_gmail_user.auth |& grep -oE '[[:xdigit:]]{16}$')
  echo trusted-key 0x$access_token_fingerprint >> $GNUPGHOME/gpg.conf



### [10. teste do token de acesso]

  # access token do usuário $msmtp_gmail_user, conforme arquivo de configuração 'gmail.yaml'
  # (ação executada pelo parâmetro 'passwordeval' do msmtp no arquivo de configuração 'msmtp.conf')
  mailctl -c gmail.yaml access $msmtp_gmail_user
  # ya29.A0AVA9y1s8VrzdVOId_KOemViy5DjdH5hdzDR8i-qHmRkxCIuvbPeuAEKlBJ-MRDUqfwy0UbKOBQHltc0Xj5BLGl0KHnzfKWlb-sP3PWnyt_AYQ4JoaaeEQOEeV13_Bff8lrYIebewwxh7sIumxmB42Hx9GcDsaCgYKATASATASFQE65dr8oyQTZfLz7M48mHUBGjV5Gw0163



### [11. teste de envio de email]

  destinatario=teste@lsd.org.br
       assunto="Olá"
      mensagem="Este e-mail é apenas um teste."

  echo -e "To: $destinatario\nSubject: $assunto\n\n$mensagem" | LC_ALL=C msmtp -C msmtp.conf -t
  cat $msmtp_dir/log
  # Nov 15 19:20:11 host=smtp.gmail.com tls=on auth=on user=$msmtp_gmail_user from=$msmtp_gmail_user recipients=teste@lsd.org.br mailsize=155 smtpstatus=250 smtpmsg='250 2.0.0 OK  1668550811 b10-20020a056870160a00b0012779ba00fesm7133835oae.2 - gsmtp' exitcode=EX_OK

  #[em caso de erro] msmtp:msmtp.conf: Permission denied
  sudo apt install -y apparmor-utils
  sudo aa-disable /etc/apparmor.d/usr.bin.msmtp



# EOF
