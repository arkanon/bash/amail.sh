# amail.sh ([ishortn.ink/amail.sh](http://ishortn.ink/amail.sh))

Envio de e-mail html com imagens inline e anexos usando o comando `msmtp` e contas seguras.

[[_TOC_]]

## Exemplos

[<img alt="aniversáro"   title="Aniversário"  height="300" src="exemplos/aniversario/screenshot.png" >](exemplos/aniversario)
[<img alt="comunicado"   title="Comunicado"   height="300" src="exemplos/comunicado/screenshot.png"  >](exemplos/comunicado)
[<img alt="pdi-ce-ok"    title="PDI-CE OK"    height="300" src="exemplos/pdi-ce/screenshot-ok.png"   >
 <img alt="pdi-ce-alert" title="PDI-CE Alert" height="300" src="exemplos/pdi-ce/screenshot-alert.png">
 <img alt="pdi-ce-error" title="PDI-CE Error" height="300" src="exemplos/pdi-ce/screenshot-error.png">](exemplos/pdi-ce)

## Uso

```
./amail.sh [arquivo de configuração]
```

O script pode ser colocado no `PATH` e renomeado ou linkado para algo mais simples, como  apenas `amail`, por exemplo.

Se não for indicado um caminho válido de arquivo de configuração, o script procurará por um arquivo chamado `amail.sh.conf` no diretório corrente.

O arquivo de configuração é um shell script em `bash` que pode/deve definir as variáveis abaixo.
Qualquer processo pode ser usado para automatizar a definição dessas variáveis. O exemplo [pdi-ce](exemplos/pdi-ce) mostra um procedimento bem completo nesse sentido.

## Variáveis

- `PREFIX  def:` diretório do arquivo de configuração usado
- `logf    def: $PREFIX/logs/amail.log      ` log das conexões com o smtp
- `talkf   def: $PREFIX/logs/amail-talk.txt ` log da comunicação entre o `msmtp` e o smtp
- `bodyf   def: $PREFIX/logs/amail-body.html` texto do email após processado para substituição dos índices da lista de valores
- `srcf    def: $PREFIX/logs/amail-src.txt  ` corpo final do email após o acréscimo dos cabeçalhos, anexos em base64 e referências internas
<!-- -->
- `smtp    (str)      ` ip ou nome do smtp
- `port    (int)      ` porta (tradicionalmente 25, 465 ou 587)
- `acct    (str)      ` conta (username, possivelmente um endereço de email)
- `pass    (str)      ` senha
<!-- -->
- `from    (str)      ` endereço de email a ser usado como remetente
- `to      (arr num)  ` lista e endereços a serem usados como destinatário
- `cc      (arr num)  ` lista e endereços a serem usados como cópia carbono
- `bcc     (arr num)  ` lista e endereços a serem usados como cópia carbono oculta
<!-- -->
- `assunto (str)      ` assunto do email. Aceita emojis e demais caracteres unicode
- `corpo   (str)      ` conteúdo do corpo do email. Se for um caminho válido para um arquivo, será usado o conteúdo do arquivo
- `inline  (arr num)  ` lista (espansível pelo shell) de arquivos que serão usados no corpo do email html
- `anexo   (arr num)  ` lista (espansível pelo shell) de arquivos que serão apresentados no email como anexos tradicionais
- `valor   (arr assoc)` lista de valores cujos índices serão substituídos pelos valores correspondendtes quando encontrados no corpo do email (para modelos de email)
<!-- -->
- `config_file   (str)` caminho de arquivo de configuração do `msmtp` em substituição aos parâmetros de smtp acima
- `account       (str)` nome da conta cadastrada no arquivo de configuração a ser usada em lugar da default

## Observações

- se o SMTP usado for o GMail, será necessário [habilitar acesso à conta por aplicativos menos seguros](http://myaccount.google.com/lesssecureapps) e [desabilitar uso de captcha para acesso a conta](http://accounts.google.com/b/0/DisplayUnlockCaptcha). Se a conta não for utilizada com uma certa freqüencia, o acesso à ela por aplicativos menos seguros será automaticamente desativado, exigindo nova habilitação manual.
- melhor que a opção acima, é criar uma [senha de aplicativo](http://myaccount.google.com/apppasswords) no GMail, mas isso exige que a conta tenha o [2FA](http://support.google.com/accounts/answer/185839) (Two Factor Authentication) habilitado.
- ainda melhor que as duas opções acima, é autenticar com [OAuth2](https://oauth.net/2/) através da ferramenta [mailctl](https://github.com/pdobsan/mailctl). O arquivo [msmtp_gmail_api.txt](msmtp_gmail_api.txt) apresenta um resumo do procedimento para autenticação do `msmtp` no GMail por OAuth2.
<!-- -->
- na atribuição do remetente, o GMail usará o nome atribuído ao endereço mas substitituirá o endereço pelo username (email) usado para autenticar no smtp.
- o ideal é que a senha da conta do SMTP seja armazenada em um arquivo separado e lida no arquivo de configuração por um dos dois métodos:

```shell-session
$ cat caminho/credential
user=nome@gmail.com
pass=senha

$ cat amail.sh.conf
...
source caminho/credential
acct=$user
# pass já foi setado no source de credential
...
```

```shell-session
$ cat caminho/credential
senha

$ cat amail.sh.conf
...
acct=nome@gmail.com
pass=$(<caminho/credential)
...
```

Dessa forma o arquivo de configuração pode ser armazenado em repositórios públicos sem comprometer a segurança, sendo necessário deixar de fora apenas o arquivo de credencial.

☐
